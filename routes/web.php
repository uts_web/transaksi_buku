<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DataController;
use App\Http\Controllers\DataMhsController;
use App\Http\Controllers\DataTransController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('beranda', function () {
//     return view('beranda');
// });
// // Route::get('add', function () {
// //     return view('data_karyawan.add');
// // });
Route::get('/', [DataController::class, 'beranda'])->name('beranda');
Route::get('datas', [DataController::class, 'show'])->name('datas');
Route::get('add', [DataController::class, 'add'])->name('add');
Route::post('datas', [DataController::class, 'prosesSimpan'])->name('datas');
Route::get('datas/edit/{id}', [DataController::class, 'edit'])->name('datas/edit/{id}');
Route::put('datas/edit/{id}', [DataController::class, 'prosesEdit'])->name('datas/edit/{id}');
Route::delete('datas/{id}', [DataController::class, 'delete'])->name('datas/{id}');

Route::get('datamhs', [DataMhsController::class, 'show'])->name('datamhs');
Route::get('addmhs', [DataMhsController::class, 'add'])->name('add');
Route::post('datamhs', [DataMhsController::class, 'prosesSimpan'])->name('datamhs');
Route::get('datamhs/editmhs/{id}', [DataMhsController::class, 'editmhs'])->name('datamhs/editmhs/{id}');
Route::put('datamhs/editmhs/{id}', [DataMhsController::class, 'prosesEdit'])->name('datamhs/editmhs/{id}');
Route::delete('datamhs/{id}', [DataMhsController::class, 'delete'])->name('datamhs/{id}');

Route::get('transs', [DataTransController::class, 'show'])->name('transs');
Route::get('tambahtran', [DataTransController::class, 'add'])->name('add');
Route::post('transs', [DataTransController::class, 'prosesSimpan'])->name('transs');
Route::get('transs/tambahtran/{id}', [DataTransController::class, 'tambahtran'])->name('transs/tambahtran/{id}');
Route::put('transs/tambahtran/{id}', [DataTransController::class, 'prosesEdit'])->name('transs/tambahtran/{id}');
Route::delete('transs/{id}', [DataTransController::class, 'delete'])->name('transs/{id}');