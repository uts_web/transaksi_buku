@extends('layout.main')
@section('title','Edit Data')
@section('breadcrumbs','MyKaryawan')

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">
                        <strong>Edit Data</strong>
                    </div>
                    <div class="pull-right">
                        <a href="{{ url('datamhs') }}" class="btn btn-secondary btn-sm">
                            <i class="fa fa-undo"></i> Back
                        </a>
                    </div>
                </div>
                <div class="card-body ">
                     <div class="row">
                         <div class="col-md-6  offset-md-3">
                             <form action="{{ url('datamhs/editmhs/'.$datamhs->id) }}" method="post">
                                @method('put')
                                @csrf
                                 <div class="form-group">
                                     <label >Nama</label>
                                     <input type="text" name="nama" class="form-control" value="{{ $datamhs->nama }}" autofocus required>
                                 </div>
                                <div class="form-group">
                                    <label >NIM</label>
                                    <input type="text" name="nim" class="form-control" value="{{ $datamhs->nim }}" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Email</label>
                                    <input type="text" name="email" class="form-control" value="{{ $datamhs->email }}"autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >No Telepon</label>
                                    <input type="date" name="no_telp" class="form-control" value="{{ $datamhs->no_telp }}" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Prodi</label>
                                    <input type="text" name="prodi" class="form-control" value="{{ $datamhs->prodi }}" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Jurusan</label>
                                    <input type="text" name="jurusan" class="form-control" value="{{ $datamhs->jurusan}}" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Fakultas</label>
                                    <input type="text" name="fakultas" class="form-control" value="{{ $datamhs->fakultas }}" autofocus required>
                                </div>
                                <button type="submit" class="btn btn-success">Update</button>
                             </form>
                         </div>
                     </div>
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection