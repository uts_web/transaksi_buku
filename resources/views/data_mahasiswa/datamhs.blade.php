@extends('layout.main')
@section('title','Data')
@section('breadcrumbs','MyKaryawan')
@section('navKaryawan','active')

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>  
        @endif
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">
                        <strong>Data Mahasiswa</strong>
                    </div>
                    <div class="pull-right">
                        <a href="/addmhs" class="btn btn-success btn-sm">
                            <i class="fa fa-plus"></i> Add Data
                        </a>
                    </div>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered table-striped table-hover table-sm">
                        <thead class="thead-dark">
                            <tr>
                                <th>No.</th>
                                <th>Nama</th>
                                <th>NIM</th>
                                <th>Email</th>
                                <th>No Telepon</th>
                                <th>Prodi</th>
                                <th>Jurusan</th>
                                <th>Fakultas</th>
                                <th>Action</th>

                            </tr>
                            
                            <tbody>
                                @foreach ($mhs as $item)
                                    <tr>
                                    <td>{{ $loop->iteration }}</td>  
                                    <td>{{ $item->nama }}</td>
                                    <td>{{ $item->nim }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->no_telp }}</td>
                                    <td>{{ $item->prodi}}</td>
                                    <td>{{ $item->jurusan }}</td>
                                    <td>{{ $item->fakultas}}</td>

                                    <td class="text-center">
                                        <a href="{{ url('datamhs/edit/'.$item->id) }}" class="btn btn-primary btn-sm">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <form action="{{ url('datamhs/'.$item->id) }}" method="post" class="d-inline" onsubmit="return confirm('Yakin Hapus Data?')">
                                            @method('delete')
                                            @csrf
                                            <button class="btn btn-danger btn-sm">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                    </tr> 
                                @endforeach
                            </tbody>
                        </thead>
                    </table>
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection