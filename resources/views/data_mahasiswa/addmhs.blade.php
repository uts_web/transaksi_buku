@extends('layout.main')
@section('title','Add Data')
@section('breadcrumbs','MyKaryawan')

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">
                        <strong>Add Data</strong>
                    </div>
                    <div class="pull-right">
                        <a href="{{ url('datamhs') }}" class="btn btn-secondary btn-sm">
                            <i class="fa fa-undo"></i> Back
                        </a>
                    </div>
                </div>
                <div class="card-body">
                     <div class="row">
                         <div class="col-md-6 offset-md-3">
                             <form action="{{ url('datamhs') }}" method="post">
                                @csrf
                                
                                 <div class="form-group">
                                     <label >Nama</label>
                                     <input type="text" name="nama" class="form-control" autofocus required>
                                 </div>
                                <div class="form-group">
                                    <label >NIM</label>
                                    <input type="text" name="nim" class="form-control" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Email</label>
                                    <input type="text" name="email" class="form-control" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >No Telepon</label>
                                    <input type="text" name="no_telp" class="form-control" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Prodi</label>
                                    <input type="text" name="prodi" class="form-control" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Jurusan</label>
                                    <input type="text" name="jurusan" class="form-control" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Fakultas</label>
                                    <input type="text" name="fakultas" class="form-control" autofocus required>
                                </div>
                                <button type="submit" class="btn btn-primary">Save</button>
                             </form>
                         </div>
                     </div>
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection