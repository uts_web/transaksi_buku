@extends('layout.main')
@section('title','Data')
@section('breadcrumbs','MyKaryawan')
@section('navKaryawan','active')

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>  
        @endif
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">
                        <strong>Data Transaksi</strong>
                    </div>
                    <div class="pull-right">
                        <a href="/tambahtran" class="btn btn-success btn-sm">
                            <i class="fa fa-plus"></i> Add Data
                        </a>
                    </div>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered table-striped table-hover table-sm">
                        <thead class="thead-dark">
                            <tr>
                                <th>No.</th>
                                <th>ID Mahasiswa</th>
                                <th>ID Buku</th>
                                <th>Tanggal Pinjam</th>
                                <th>Tanggal Kembali</th> 
                                <th>Status Pinjam</th>
                                <th>Total Biaya</th> 
                                <th>Action</th>      

                            </tr>
                            
                            <tbody>
                                @foreach ($tsi as $item)
                                    <tr>
                                    <td>{{ $loop->iteration }}</td>  
                                    <td>{{ $item->id_mahasiswa }}</td>
                                    <td>{{ $item->id_buku }}</td>
                                    <td>{{ $item->tanggal_pinjam }}</td>
                                    <td>{{ $item->tanggal_kembali }}</td>
                                    <td>{{ $item->status_pinjam }}</td>
                                    <td>{{ $item->total_biaya}}</td>

                                    <td class="text-center">
                                        <a href="{{ url('transs/edit/'.$item->id) }}" class="btn btn-primary btn-sm">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <form action="{{ url('transs/'.$item->id) }}" method="post" class="d-inline" onsubmit="return confirm('Yakin Hapus Data?')">
                                            @method('delete')
                                            @csrf
                                            <button class="btn btn-danger btn-sm">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                    </tr> 
                                @endforeach
                            </tbody>
                        </thead>
                    </table>
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection