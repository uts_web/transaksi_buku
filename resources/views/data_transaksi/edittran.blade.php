@extends('layout.main')
@section('title','Edit Data')
@section('breadcrumbs','MyKaryawan')

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">
                        <strong>Edit Data</strong>
                    </div>
                    <div class="pull-right">
                        <a href="{{ url('transs') }}" class="btn btn-secondary btn-sm">
                            <i class="fa fa-undo"></i> Back
                        </a>
                    </div>
                </div>
                <div class="card-body ">
                     <div class="row">
                         <div class="col-md-6  offset-md-3">
                             <form action="{{ url('transs/edit/'.$datas->id) }}" method="post">
                                @method('put')
                                @csrf
                                 <div class="form-group">
                                     <label >ID Mahasiswa</label>
                                     <input type="text" name="id_mahasiswa" class="form-control" value="{{ $transs->id_mahasiswa }}" autofocus required>
                                 </div>
                                <div class="form-group">
                                    <label >ID Buku</label>
                                    <input type="text" name="id_buku" class="form-control" value="{{ $transs->id_buku }}" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Tanggal Pinjam</label>
                                    <input type="date" name="tanggal_pinjam" class="form-control" value="{{ $transs->tanggal_pinjam }}"autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Tanggal Kembali</label>
                                    <input type="date" name="tanggal_kembali" class="form-control" value="{{ $transs->tanggal_kembali }}" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Status Pinjam</label>
                                    <input type="text" name="status_pinjam" class="form-control" value="{{ $transs->status_pinjam }}" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Total Biaya</label>
                                    <input type="decimal" name="total_biaya" class="form-control" value="{{ $transs->total_biaya}}" autofocus required>
                                </div>
                                <button type="submit" class="btn btn-success">Update</button>
                             </form>
                         </div>
                     </div>
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection