@extends('layout.main')
@section('title','Add Data')
@section('breadcrumbs','MyKaryawan')

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">
                        <strong>Add Data</strong>
                    </div>
                    <div class="pull-right">
                        <a href="{{ url('transs') }}" class="btn btn-secondary btn-sm">
                            <i class="fa fa-undo"></i> Back
                        </a>
                    </div>
                </div>
                <div class="card-body">
                     <div class="row">
                         <div class="col-md-6 offset-md-3">
                             <form action="{{ url('transs') }}" method="post">
                                @csrf
                                
                                 <div class="form-group">
                                     <label >ID Mahasiswa</label>
                                     <input type="number" name="id_mahasiswa" class="form-control" autofocus required>
                                 </div>
                                <div class="form-group">
                                    <label >ID Buku</label>
                                    <input type="number" name="id_buku" class="form-control" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Tanggal Pinjam</label>
                                    <input type="date" name="tanggal_pinjam" class="form-control" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Tanggal Kembali</label>
                                    <input type="date" name="tanggal_kembali" class="form-control" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Status Pinjam</label>
                                    <input type="text" name="status_pinjam" class="form-control" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Total Biaya</label>
                                    <input type="decimal" name="total_biaya" class="form-control" autofocus required>
                                </div>
                                <button type="submit" class="btn btn-primary">Save</button>
                             </form>
                         </div>
                     </div>
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection