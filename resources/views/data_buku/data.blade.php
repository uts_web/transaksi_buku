@extends('layout.main')
@section('title','Data')
@section('breadcrumbs','MyKaryawan')
@section('navKaryawan','active')

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>  
        @endif
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">
                        <strong>Data Buku</strong>
                    </div>
                    <div class="pull-right">
                        <a href="/add" class="btn btn-success btn-sm">
                            <i class="fa fa-plus"></i> Add Data
                        </a>
                    </div>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered table-striped table-hover table-sm">
                        <thead class="thead-dark">
                            <tr>
                                <th>No.</th>
                                <th>Judul Buku</th>
                                <th>Pengarang</th>
                                <th>Penerbit</th>
                                <th>Tahun Terbit</th> 
                                <th>Tebal</th>
                                <th>ISBN</th> 
                                <th>Stok Buku</th>
                                <th>Biaya Sewa Harian (Rp)</th>  
                                <th>Action</th>      

                            </tr>
                            
                            <tbody>
                                @foreach ($b as $item)
                                    <tr>
                                    <td>{{ $loop->iteration }}</td>  
                                    <td>{{ $item->judul_buku }}</td>
                                    <td>{{ $item->pengarang }}</td>
                                    <td>{{ $item->penerbit }}</td>
                                    <td>{{ $item->tahun_terbit }}</td>
                                    <td>{{ $item->tebal }}</td>
                                    <td>{{ $item->isbn}}</td>
                                    <td>{{ $item->stok_buku }}</td>
                                    <td>{{ $item->biaya_sewa_harian}}</td>

                                    <td class="text-center">
                                        <a href="{{ url('datas/edit/'.$item->id) }}" class="btn btn-primary btn-sm">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <form action="{{ url('datas/'.$item->id) }}" method="post" class="d-inline" onsubmit="return confirm('Yakin Hapus Data?')">
                                            @method('delete')
                                            @csrf
                                            <button class="btn btn-danger btn-sm">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                    </tr> 
                                @endforeach
                            </tbody>
                        </thead>
                    </table>
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection