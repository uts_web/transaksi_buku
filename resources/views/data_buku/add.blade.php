@extends('layout.main')
@section('title','Add Data')
@section('breadcrumbs','MyKaryawan')

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">
                        <strong>Add Data</strong>
                    </div>
                    <div class="pull-right">
                        <a href="{{ url('datas') }}" class="btn btn-secondary btn-sm">
                            <i class="fa fa-undo"></i> Back
                        </a>
                    </div>
                </div>
                <div class="card-body">
                     <div class="row">
                         <div class="col-md-6 offset-md-3">
                             <form action="{{ url('datas') }}" method="post">
                                @csrf
                                
                                 <div class="form-group">
                                     <label >Judul Buku</label>
                                     <input type="text" name="judul" class="form-control" autofocus required>
                                 </div>
                                <div class="form-group">
                                    <label >Pengarang</label>
                                    <input type="text" name="pengarang" class="form-control" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Penerbit</label>
                                    <input type="text" name="penerbit" class="form-control" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Tahun Terbit</label>
                                    <input type="date" name="terbit" class="form-control" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Tebal</label>
                                    <input type="text" name="tebal" class="form-control" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >ISBN</label>
                                    <input type="text" name="isbn" class="form-control" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Stok Buku</label>
                                    <input type="decimal" name="stok" class="form-control" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Biaya Sewa Harian (Rp)</label>
                                    <input type="decimal" name="biaya" class="form-control" autofocus required>
                                </div>
                                <button type="submit" class="btn btn-primary">Save</button>
                             </form>
                         </div>
                     </div>
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection