@extends('layout.main')
@section('title','Edit Data')
@section('breadcrumbs','MyKaryawan')

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">
                        <strong>Edit Data</strong>
                    </div>
                    <div class="pull-right">
                        <a href="{{ url('datas') }}" class="btn btn-secondary btn-sm">
                            <i class="fa fa-undo"></i> Back
                        </a>
                    </div>
                </div>
                <div class="card-body ">
                     <div class="row">
                         <div class="col-md-6  offset-md-3">
                             <form action="{{ url('datas/edit/'.$datas->id) }}" method="post">
                                @method('put')
                                @csrf
                                 <div class="form-group">
                                     <label >Judul Buku</label>
                                     <input type="text" name="judul" class="form-control" value="{{ $datas->judul_buku }}" autofocus required>
                                 </div>
                                <div class="form-group">
                                    <label >Pengarang</label>
                                    <input type="text" name="pengarang" class="form-control" value="{{ $datas->pengarang }}" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Penerbit</label>
                                    <input type="text" name="penerbit" class="form-control" value="{{ $datas->penerbit }}"autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Tahun Terbit</label>
                                    <input type="date" name="terbit" class="form-control" value="{{ $datas->tahun_terbit }}" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Tebal</label>
                                    <input type="text" name="tebal" class="form-control" value="{{ $datas->tebal }}" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >ISBN</label>
                                    <input type="text" name="isbn" class="form-control" value="{{ $datas->isbn}}" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Stok Buku</label>
                                    <input type="decimal" name="stok" class="form-control" value="{{ $datas->stok_buku }}" autofocus required>
                                </div>
                                <div class="form-group">
                                    <label >Biaya Sewa Harian (Rp)</label>
                                    <input type="decimal" name="biaya" class="form-control" value="{{ $datas->biaya_sewa_harian }}" autofocus required>
                                </div>
                                <button type="submit" class="btn btn-success">Update</button>
                             </form>
                         </div>
                     </div>
                </div>
            </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection