<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataTransController extends Controller
{
    public function beranda ()
    {
        return view ('beranda');
    }
    public function show ()
    {
        $transaksi = DB::table('transaksi')->get();

        return view ('data_transaksi.transaksi',['tsi'=>$transaksi]);
    }
    public function add ()
    {
        return view ('data_transaksi.tambahtran');
    }

    public function prosesSimpan (Request $req)
    {
      
        DB::table('transaksi')->insert([
            'id_mahasiswa'=>$req->id_mahasiswa,
            'id_buku'=>$req->id_buku,
            'tanggal_pinjam'=>$req->tanggal_pinjam,
            'tanggal_kembali'=>$req->tanggal_kembali,
            'status_pinjam'=>$req->status_pinjam,
            'total_biaya'=>$req->total_biaya,
            ]);

       
        return redirect('/transs')->with('status', 'Data Berhasil Ditambah!');
    }

    public function edit($id)
    {
        $datas = DB::table('transaksi')->where('id', $id)->first();
      
        return view ('data_transaksi.edittran', compact('transs'));
    }

    public function prosesEdit(Request $req, $id)
    {
        DB::table('transaksi')->where('id', $id)
            ->update([
            'id_mahasiswa'=>$req->id_mahasiswa,
            'id_buku'=>$req->id_buku,
            'tanggal_pinjam'=>$req->tanggal_pinjam,
            'tanggal_kembali'=>$req->tanggal_kembali,
            'status_pinjam'=>$req->status_pinjam,
            'total_biaya'=>$req->total_biaya,
             ]);
      
        return redirect('/transs')->with('status', 'Data Berhasil Diupdate!');
    }

    public function delete($id)
    {
       DB::table('transaksi')->where('id',$id )->delete();
       return redirect('/transs')->with('status', 'Data Berhasil Dihapus!');
    }
} 
