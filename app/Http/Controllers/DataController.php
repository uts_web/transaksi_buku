<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataController extends Controller
{
    public function beranda ()
    {
        return view ('beranda');
    }
    public function show ()
    {
        $buku = DB::table('buku')->get();

        return view ('data_buku.data',['b'=>$buku]);
    }
    public function add ()
    {
        return view ('data_buku.add');
    }

    public function prosesSimpan (Request $req)
    {
      
        DB::table('buku')->insert(
            ['judul_buku'=>$req->judul,
             'pengarang'=>$req->pengarang,
             'penerbit'=>$req->penerbit,
             'tahun_terbit'=>$req->terbit,
             'tebal'=>$req->tebal,
             'isbn'=>$req->isbn,
             'stok_buku'=>$req->stok,
             'biaya_sewa_harian'=>$req->biaya,

            ]);

       
        return redirect('/datas')->with('status', 'Data Berhasil Ditambah!');
    }

    public function edit($id)
    {
        $datas = DB::table('buku')->where('id', $id)->first();
      
        return view ('data_buku.edit', compact('datas'));
    }

    public function prosesEdit(Request $req, $id)
    {
        DB::table('buku')->where('id', $id)
                    ->update([
                        'judul_buku'=>$req->judul,
                        'pengarang'=>$req->pengarang,
                        'penerbit'=>$req->penerbit,
                        'tahun_terbit'=>$req->terbit,
                        'tebal'=>$req->tebal,
                        'isbn'=>$req->isbn,
                        'stok_buku'=>$req->stok,
                        'biaya_sewa_harian'=>$req->biaya,
                    ]);
      
        return redirect('/datas')->with('status', 'Data Berhasil Diupdate!');
    }

    public function delete($id)
    {
       DB::table('buku')->where('id',$id )->delete();
       return redirect('/datas')->with('status', 'Data Berhasil Dihapus!');
    }
} 
