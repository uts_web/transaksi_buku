<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataMhsController extends Controller
{
    public function beranda ()
    {
        return view('beranda');
    }
    public function show ()
    {
        $mahasiswa = DB::table('mahasiswa')->get();

        return view ('data_mahasiswa.datamhs',['mhs'=>$mahasiswa]);
    }
    public function add ()
    {
        return view ('data_mahasiswa.addmhs');
    }

    public function prosesSimpan (Request $req)
    {
      
        DB::table('mahasiswa')->insert(
            ['nama'=>$req->nama,
             'nim'=>$req->nim,
             'email'=>$req->email,
             'no_telp'=>$req->no_telp,
             'prodi'=>$req->prodi,
             'jurusan'=>$req->jurusan,
             'fakultas'=>$req->fakultas,

            ]);

       
        return redirect('/datamhs')->with('status', 'Data Berhasil Ditambah!');
    }

    public function edit($id)
    {
        $datamhs = DB::table('mahasiswa')->where('id', $id)->first();
      
        return view ('data_mahasiswa.editmhs', compact('datamhs'));
    }

    public function prosesEdit(Request $req, $id)
    {
        DB::table('mahasiswa')->where('id', $id)
                    ->update([
                        'nama'=>$req->nama,
                        'nim'=>$req->nim,
                        'email'=>$req->email,
                        'no_telp'=>$req->no_telp,
                        'prodi'=>$req->prodi,
                        'jurusan'=>$req->jurusan,
                        'fakultas'=>$req->fakultas,
                    ]);
      
        return redirect('/datamhs')->with('status', 'Data Berhasil Diupdate!');
    }

    public function delete($id)
    {
       DB::table('mahasiswa')->where('id',$id )->delete();
       return redirect('/datamhs')->with('status', 'Data Berhasil Dihapus!');
    }
} 
